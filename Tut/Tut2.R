####Tutorial_2####
sample(c("h","t"),10,replace=T,c(0.6,0.4))
####
set.seed(123)
R<-10
D<-rnorm(100)
sampled<-numeric(1000)
for(i in 1:1000){
  sampled[i]<-sum(sample(D,R))
}

hist(sampled, border = "steelblue")
abline(v=mean(D), lwd=2,col='gold')
#### CROSS VALIDATION #####
###MSE using linear regression###
x<- rnorm(500,3,5)
y<- x**2
K<-5
group <-rep(1:K, length=500)
group <- sample(group)
mse.group <- numeric(5)
for(i in 1:K){
  fit.lm <- lm(y[group !=i] ~ x[group != i])
  
}