5+5
3*exp(2)+log(10)/sqrt(2)
getwd()
setwd("C:/Users/PHS/Desktop/CoDi/Ex")
help("setwd")
?setwd
install.packages("survival")
library(survival)
data(cancer)
load("")
datasets<- read.table(file ="",header=T,sep = "," )
x<-(1+1/10%%3)^100
y<-(2^100)
sum(1,2,3,4,5)
prod(1:5)
factorial(5)
typeof(7.5)
as.integer(3.2)
x<-c(1, 2, 3, 3, 5)
x<- 1:4
y<-c("Tampere", "Helsinki")
z<-c("Tampere", "Helsinki", 10)
z<-list(first = "Morteza", second="beigi", age=29)
is.character(z$first)
is.numeric(z$age)
vec<-seq(from = 0.1, to = 10, by=0.1)
length(vec)
mat<-matrix(vec, ncol= 10)
mat[5,3]
mat[5:8, 4]
mat[5:8, 4:6]
offspring<- list(dog=c(2,5,3,8), cat = c(12,6,7,4), snake = c(43,35,23,10))
mylist<-list(x=matrix(c(offspring$dog,offspring$cat,offspring$snake),ncol=3),y=10,z=offspring)
mylist[c("x","y")]
mylist[1:2]
myDf<-data.frame(pet = c(rep("dog",4),rep("cat",4),rep("snake",4)),offspring = unlist(offspring))
plot(myDf, col=rainbow(3))
sqrFun <- function(x){
  y <- x^2
  return(y)
}
sqrFun(10)

newFun<-function(data, operation="sum"){
  if(operation=="sum"){
    output<-sum(data)
  }
  else{
    output<-prod(data)
  }
  return(output)
}
newFun(offspring$dog, "sum")
newFun(offspring$dog, "product")

x<-seq(from=10, to=100, by=10)
for(i in x){
  print(i)
}
rows <- 5
cols<- 3
mat_data<-matrix(0, nrow = rows, ncol = cols)
for(i in 1:rows){
  s<-sample(2:8, cols, replace=T)
  mat_data[i,]<-s
}
mat_data
d<-c(11,12,13,14,15)
for(i in 1:length(d)){
  if (d[i]=="13")
    print(i)
}
which(d=='13')
which.max(d)
x<-10
while(x<=100){
  print(x)
  x<-x+10
}
R<-10
D<-rnorm(50)
sampled<-sample(D,R)

R<-10
D<-rnorm(50,5,2)
plot(D)
sampled<-sample(D,R,replace=T)
plot(sampled)
set.seed(2)
R<-10
D<-rpois(100,lambda = 3)
sampled<-sample(D,R)
D<-runif(100,min = 1,max=4)
sampled<-sample(D,R)
sample(1:20,5)
set.seed(120)
sample(1:20,5)
#
#PRACTICAL EXERCISES
#EX1 and EX2
53+12
sqrt(225)
540/16
log(500)
log10(2)
factorial(6)
1/(2.0*10^-3)
1/2e-3
seq(from=1, to=10, by=1)
s<-1:10
y<-s^2
#EX3
w<-y[7]
y5<-y[5]
x5<-s[5]
x5==sqrt(y5)
#EX4
rand<-rnorm(100,5,1)
R<-20
new_sampled<-sample(rand,R)
typeof(new_sampled)
class(rand)
is.vector(rand)
length(rand)
mean(rand)
sd(rand)
