####MORTEZA MOHAMMADKHANBEIGI - 223998 ####
###morteza.mohammadkhanbeigi@student.tut.fi###
####EX_4####
data1 <- 1:10
new_sampled1<-sample(data1,10, replace = TRUE)
unique_data1 = unique(new_sampled1)
estimated_percentage1 = (length(unique_data1)/length(new_sampled1))*100

data2 <- 1:100
new_sampled2<-sample(data2,100, replace = TRUE)
unique_data2 = unique(new_sampled2)
estimated_percentage2 = (length(unique_data2)/length(new_sampled2))*100

data3 <- 1:1000
new_sampled3<-sample(data3,1000, replace = TRUE)
unique_data3 = unique(new_sampled3)
estimated_percentage3 = (length(unique_data3)/length(new_sampled3))*100

data4 <- 1:10000
new_sampled4<-sample(data4,10000, replace = TRUE)
unique_data4 = unique(new_sampled4)
estimated_percentage4 = (length(unique_data4)/length(new_sampled4))*100

#######################################################################

vec_sizes <-c(10,100,1000,10000) 
vec_estimated <- c(estimated_percentage1,estimated_percentage2,estimated_percentage3,estimated_percentage4)
plot(vec_sizes, vec_estimated, ylab = "Estimated Percentage", xlab = "Size")
