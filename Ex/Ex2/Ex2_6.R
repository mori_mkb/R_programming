####MORTEZA MOHAMMADKHANBEIGI - 223998 ####
###morteza.mohammadkhanbeigi@student.tut.fi###
####EX_6####

D<-rnorm(100,0.6,1)

mat_data = matrix(0, nrow = 1000, ncol = 100)
mat_data_mean = matrix(0, nrow = 1000, ncol = 1)

for(i in 1:1000){
  s<-sample(D,100, replace=T)
  mat_data[i,]<-s
}

for(i in 1:1000){
  mat_data_mean[i,]<-mean(mat_data[i,])
}

hist(mat_data_mean,col = "red", breaks = 40)

pdf("Ex2_6_plot.pdf", width = 5, height = 6)
hist(mat_data_mean,col = "red", breaks = 40)
dev.off()