####MORTEZA MOHAMMADKHANBEIGI - 223998 ####
###morteza.mohammadkhanbeigi@student.tut.fi###
####EX_5####

D<-rnorm(1000,0.6,1)
hist(D,col = "red", breaks = 40)
abline(v = 0.6, col="green")
pdf("Ex2_5_plot.pdf", width = 5, height = 6)
hist(D,col = "red", breaks = 40)
abline(v = 0.6, col="green")
dev.off()